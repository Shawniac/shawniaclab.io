# Sources

## Posts

### fib_calc

- [Featured Image](https://de.wikipedia.org/wiki/Pascalsches_Dreieck#/media/Datei:PascalTriangleFibanacci.svg)
  - License: CC BY-SA 3.0
  - File: pascal_triangle_fibonacci.png

### initial_commit

- [Featured Image](https://www.pexels.com/photo/black-pen-on-white-writing-spring-notebook-between-white-ipad-and-white-ceramic-mug-with-latte-on-white-plate-163187/)
  - License: CC0
  - File: black_pen_notebook_coffee.png

### space_trader

- [Featured Image](https://gitlab.com/Shawniac/space_trader/-/blob/main/screenshots/screen_01.webp?ref_type=heads)
  - License: CC0
  - File: space_trader.png

### game_dev_engines

- [Featured Image](https://www.bing.com/images/create/a-fight-scene-where-one-blue-robot-in-the-center-f/650fdaa1ce03463aa2a5a149304e9614?id=bP%2basP%2fJS1j%2fSxnm2KUPgA%3d%3d&view=detailv2&idpp=genimg&FORM=GCRIDP&mode=overlay)
  - License: CC0
  - File: game_dev_engines.png
