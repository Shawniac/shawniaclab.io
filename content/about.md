---
title: "About"
subtitle: "Who is Jan Benda 🤔"
date: 2023-09-17T16:36:32+02:00
lsmod: 2023-09-17T20:10:48+02:00
draft: false
author: "Jan Benda"
authorLink: ""
description: ""
license: "CC BY-NC 4.0"
images: []

tags: [personal, education, work]
categories: [other]
---

## Intro

I'm a Software Developer who loves emulation, linux, gaming, machine learning, rust and python.

## Experience

![the associated engineers group](/images/about/companies/taeg.webp)

- job title: Partner / Developer Team-Lead
- company name: [The Associated Engineers Group (TAEG)](https://associatedengineersgroup.com/)
- duration: 3 months (2023 - 2024)

---

![websale ag](/images/about/companies/websale.webp)

- job title: C++ Developer
- company name: [WEBSALE AG](https://websale.de/)
- duration: 4 years (2019 - 2023)

## Education

![fau erlangen-nürnberg](/images/about/education/fau.webp)

- title: Bachelor of Science (Bsc)
- field: Information and Communication Technology (ICT)
- institute: [Friedrich-Alexander-Universität Erlangen-Nürnberg (FAU)](https://www.fau.de/)
- duration: 4 years (2013 - 2017)
