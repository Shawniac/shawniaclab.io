---
title: "Initial Commit"
subtitle: "Trying out blogging"
date: 2022-10-16T18:52:05+02:00
lastmod: 2023-09-17T14:03:32+02:00
draft: false
author: "Jan Benda"
authorLink: ""
description: ""
license: "CC BY-NC 4.0"
images: []

tags: [hugo, intro]
categories: [blog]

featuredImage: "/images/posts/features/black_pen_notebook_coffee.webp"
---

I chose this incredibly creative headline because that's a common first commit message to any new git project.
This Website was created a few years back (way back in 2017) but never really received any care or attention at all,
to be honest I forgot about it for a long time.

There was simply no time, or at least that's what I felt like, to jolt down a few lines of text in the evening after work and family time.

Because of the recent state of the world, the shift towards mental health, being mindful, and all the troubles we are all going through (politicians going crazy, corona virus, ukraine war, etc.) at this time I decided to pick up personal character development and mindfulness in the form of blogging.

So here it is! My plan is to get started with blogging about the stuff I am doing to build for work or in private projects to build a portfolio, instead of it all getting lost in the pile of unfinished and unmentioned projects.

<!-- markdownlint-disable MD026 -->
### Why Hugo? :eyes:

> Why even use a _site builder_? If you known the basics of web development, why not just write it all yourself?

All the Content is written in Markdown, which makes it much easier to just write the content, not having to think about the formatting, and that means one less thing to worry about and easier flow of the creative juices.

And why did I choose Hugo vs. `<shiny popular framework here>`? To be honest I just googled website frameworks and stuck with the first that was easy to use and worked, I guess that's due to my engineering background, if it works it works. :shrug:

### Why start blogging? :thinking:

A very good question. For one it would allow me to showcase my skills more than just a portfolio alone could, since not everything fits under the umbrella of a portfolio, but I can also use it to blog about whatever and express myself about things I care about. Secondly my blog is meant to act as a guide on how to do things, if I ever would like to do it again. And maybe some day even random people on the internet.

### So what can I expect to see in this blog? :star-struck:
<!-- markdownlint-enable MD026 -->
A random collection of stuff that I found interesting and want to know more about or invest time by making a project with some app/library/technology/whatever that I came across.

Probable Topics include (but are not limited to):

- Game Development
  - Godot Engine (OSS, python like scripting)
  - Bevy (ECS, Rust)
- General Python and Rust Development
  - Stuff I come across in private or work projects
- Microcontrollers
  - I want to experiment with this like the MicroPython (want to buy) board or Espruino (collecting dust in a drawer)
- Web Development
  - Everything I come across while writing this blog
