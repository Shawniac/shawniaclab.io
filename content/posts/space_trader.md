---
title: "Space Trader Game"
subtitle: "A basic 2D Game made with Godot Engine about being a Trader in Space"
date: 2022-10-18T16:18:11Z
lastmod: 2023-09-17T14:03:32+02:00
draft: false
author: "Jan Benda"
authorLink: ""
description: ""
license: "CC BY-NC 4.0"
images: []

tags: [godot, oss]
categories: [game-development]

featuredImage: "/images/posts/features/space_trader.webp"
---

## Space Trader: A never-finished space trading game

I've long been a fan of the [X game series](https://en.wikipedia.org/wiki/X_(video_game_series)), all the way back when I saw a physical CD with *X: Beyond the Frontier* written on it. Since playing that game as a little kid I've always dreaming on making something like that myself one day.
So a few years ago, I decided to start developing my own little 2d space trading game, which I creatively called *Space Trader*.

The basic idea of the game was to have a top down space trading game, with a strong focus on trading and management. You would start with a small ship and a small amount of money, and you would have to manually buy and sell goods to make a profit. As you made more money, you could buy better ships and carry more goods.

Eventually, you would be able to contract and automate the trading, and you could start to build trade routes. You would also need to protect your traders from pirates and other threats. The goal is to raise an empire and dominate the galaxies for the maximum profit and power.

The game would have a variety of interesting mechanics, including:

* A dynamic economy: The prices of goods would fluctuate based on supply and demand, and you would need to be strategic about when you bought and sold.
* A variety of different ship types: Each ship type would have its own strengths and weaknesses, so you would need to choose the right ships for your trade routes.
* A variety of different goods to trade: There would be a wide variety of different goods to trade, each with its own unique properties.
* A research system: You could research new technologies to improve your ships and trade routes.
* A diplomacy system: You could negotiate trade agreements with other factions, and you could even go to war with them if necessary.

I never finished Space Trader, but I think it has the potential to be a really fun and challenging game. Here are a few suggestions for a name I got from Google's Bard:

* Galactic Trader
* Star Merchant
* Space Tycoon
* Trade Winds
* Interstellar Emporium

I hope this blog post has given you some ideas for your own space trading game or at least was worth your while.
One day I hope I advanced far enough as a game developer to come back and finish this game.

Thanks for reading, and happy game developing!

## Source

[Gitlab Repository](https://gitlab.com/Shawniac/space_trader)
