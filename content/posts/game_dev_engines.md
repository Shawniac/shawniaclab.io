---
title: "Exploring the Game Engine / Framework Landscape: My Personal Journey of Finding the Perfect Fit"
subtitle: "Join me on my quest to find the ideal game engine or framework as a solo developer. In this blog post, I'll share my experience with each of them, including Unreal Engine, LibGDX, Phaser, LÖVE, and ultimately, Godot."
date: 2023-09-19T22:57:01+02:00
lastmod: 2023-09-19T22:57:01+02:00
draft: false
author: "Jan Benda"
license: "CC BY-NC 4.0"

tags: [godot, ai, python]
categories: [game-development]

featuredImage: "/images/posts/features/game_dev_engines.webp"
---

> DISCLAIMER: This post was written with the help of [Google's Bard](https://bard.google.com/chat/)
>
> I wanted to know how good Bard is for creative stuff like writing blog posts, and I have to say I am pretty impressed! Especially if you want to quickly produce content and only proofread it a few times, it can be a huge help.
>
> And yes I do realize that this also means, that every common schmo can generate a lot of content that might be questionable at best, but that's a whole can of worms I don't want to open in this post. I view generative AI as a knife, a very useful tool in the right hands for cooking for example, but a deadly weapon in the wrong hands.

As a solo game developer, I've always been on a quest to find the perfect game engine that fits my needs. I've tried a few different ones over the years, from the industry leaders to the up-and-coming contenders. Some have been good, but none have been truly great - until I finally found **the one**.

## The Engines

### Unity

[Unity](https://unity.com/) is a popular game engine that has been widely used by developers for creating games across various platforms. While it has many positive aspects, there have been some notable missteps along the way.

One of the recent controversies involves Unity’s introduction of a new pricing model called the [Unity Runtime Fee](https://www.ign.com/articles/why-unitys-new-install-fees-are-spurring-massive-backlash-among-game-developers). This policy has faced significant backlash from game developers, as it imposes a small fee on each game download built on Unity’s engine. The fee is set to take effect in January 2024.

Another criticism Unity faced was its partnership with an advertisement company that was involved in [unethical practices](https://www.pcgamer.com/unity-is-merging-with-a-company-who-made-a-malware-installer/). These incidents have raised concerns among developers and have led to widespread discussions about the future of Unity as a game development platform.

Please note that while these negative points exist, Unity remains a powerful and widely used game engine with a large community of developers. It offers various features and tools that can help create impressive games across different genres and platforms.

I just personally never found Unity to be a good fit for me as a solo developer, and never liked their business model to begin with. Also, they don't have Linux support for their editor, which sucks!

**UPDATE(2023-09-24):** Unity has backpedaled on their pricing model and have made [adjustments](https://www.theverge.com/2023/9/22/23882768/unity-new-pricing-model-update).

### Unreal Engine

[Unreal Engine](https://www.unrealengine.com/) is a cross-platform game engine developed by Epic Games. It is one of the most popular game engines in the world, and it is used to create a wide variety of games, including AAA titles such as Fortnite, Gears of War, and Unreal Tournament.

Unreal Engine is known for its high-quality graphics and its wide range of features. It includes a built-in level editor, animation tools, a physics engine, and a scripting language. Unreal Engine also has a large community of developers and a wide range of assets and plugins available.

However, Unreal Engine can be complex and challenging to learn, and it is not the best choice for solo developers or beginners. It is also a resource-intensive engine, so it requires a powerful computer to run smoothly.

### LibGDX

[LibGDX](https://libgdx.com/) is a Java framework for game development. It is less feature-rich than Unreal Engine, but it is also more flexible and lightweight. This makes it a good choice for developers who want to have more control over the development process or who are targeting specific platforms, such as mobile devices. LibGDX is also a good choice for developers who are already familiar with Java.

LibGDX has many features that make it easy to develop games, such as a built-in scene graph, physics engine, and input handling. It also has a large community of developers and a wide range of libraries available.

However, LibGDX does not include a built-in level editor or animation tools. Developers need to use third-party tools or create their own. LibGDX is also not as well-known as Unreal Engine, so it can be more difficult to find help and resources.

### Phaser

[Phaser](https://phaser.io/) is a JavaScript framework for developing web games. It is easy to learn and use, and it has a large community of developers. Phaser is a good choice for developers who want to create simple 2D games or who want to target web browsers. However, it is not as powerful as Unreal Engine or LibGDX, and it may not be suitable for complex games.

Phaser is great for developing smaller web-based games and includes the common features you'd except, such as a built-in scene graph, physics engine, and animation tools. It also has a large library of sprites and other assets available.

However, Phaser is not as well-suited for developing games for other platforms, such as mobile devices or consoles. It is also not as powerful as Unreal Engine or LibGDX, so it may not be suitable for complex games.

The popular Game [Vampire Survivors](https://store.steampowered.com/app/1794680/Vampire_Survivors/) was initially made with Phaser, but recently the Developer moved on to Unity, because of the performance drawbacks of using Phaser.


### LÖVE

[LÖVE](https://love2d.org/), also known as love2d, is a free and open-source game development framework for 2D games using the Lua programming language. It is designed to be simple, lightweight, and beginner-friendly, making it an excellent choice for solo game developers.

With LÖVE, solo game developers can focus on creating their games without worrying about low-level details. The engine takes care of tasks such as window management, input handling, and rendering, allowing developers to quickly prototype and iterate on their ideas.

One of the key features of LÖVE is its simplicity. The engine provides a minimalistic API that is easy to learn and use, making it accessible to developers of all skill levels. This simplicity, combined with the power of Lua, allows solo game developers to bring their ideas to life with minimal effort.

Another advantage of using LÖVE as a solo game developer is the active and supportive community. The LÖVE community is known for its helpfulness and willingness to assist newcomers.

In conclusion, LÖVE is an ideal choice for solo game developers looking to create 2D games with a preference for the Lua language. Its simplicity, lightweight nature, and active community make it a powerful tool for bringing game ideas to life.

### Godot Engine

[Godot Engine](https://godotengine.org/) is a complete open-source game development environment that is designed for both solo developers and small teams. It is easy to learn and use, but it is also powerful enough to create complex games. Godot also has a built-in Python-like scripting language, which is a major plus for many developers. Godot is a good choice for developers who are looking for a complete game development solution or who want to create games for a variety of platforms, including PC, mobile, web, and even [XR](https://en.wikipedia.org/wiki/Extended_reality).

Godot includes several features that make it easy to develop games, such as a built-in level editor, animation tools, a physics engine, and a scripting language. It also has a large community of developers and a wide range of assets and plugins available.

However, Godot is not as well-known as Unreal Engine, so it can be more difficult to find help and resources. It is also not as well-suited for developing AAA games.

## Why I ultimately chose Godot

I chose Godot Engine for several reasons:

- it's easy to learn and use, which is important for a solo developer who does not have a lot of time to invest in learning a complex game engine
- their editor's Linux support is also first class and the engine itself even works in the browser
- it has a built-in Python-like scripting language, which is a major plus for me as I already have lots of Python experience
- it is a good choice for developing games for a variety of platforms, which is important for me as I want to be able to publish my games to a wide audience
- it is open source and they don't have any costs attached to it or try to impose fees on you (looking at you Unity)

Overall, I believe that Godot Engine is a great choice for solo developers who are looking for a powerful and easy-to-use game engine

**And they also "recently" (March 2023) released Godot 4, which is a major step up from Godot 3.**

It includes several improvements and new features, such as:

- a new rendering system with Vulkan support
- improved GDScript performance and usability
- enhanced physics and animation systems
- a new 2D tilemap editor

For more info check out [their awesome article](https://godotengine.org/article/godot-4-0-sets-sail/). (I highly recommend going to their blog regularly)
